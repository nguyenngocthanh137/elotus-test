import { useState, createContext, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import {
	AppContextType,
	RequestData,
	RequestDataList,
	ResultsItem,
} from "../type/type";

import { handleLazyImage } from "../helpers/handleLazyImage";

import {
	getNowPlayingApi,
	getTopRatedApi,
	getSearchApi,
	getDetailMovie,
} from "../utils/api";

import { ViewType, PathType } from "../type/enum";

const imageURL = process.env.REACT_APP_API_IMAGE;

export const AppContext = createContext<null | AppContextType>(null);

type PropsType = {
	children: React.ReactNode;
};

const AppProvider: React.FC<PropsType> = ({ children }) => {
	const location = useLocation();
	const navigate = useNavigate();

	const [viewType, setViewType] = useState(ViewType.grid);
	const [data, setData] = useState<RequestDataList | any>(null);
	const [detail, setDetail] = useState<ResultsItem | any>(null);
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState([]);
	const [showError, setShowError] = useState(false);

	const handleGetData = async (data: RequestData) => {
		let res;
		setLoading(true);
		if (data.kind === PathType.nowPlaying) {
			res = await getNowPlayingApi(data?.params);
		} else if (data.kind === PathType.topRated) {
			res = await getTopRatedApi(data?.params);
		} else if (data.kind === PathType.search) {
			res = await getSearchApi(data?.params);
		}

		if (!res?.success) {
			setData([]);
			setError(res?.errors);
			setShowError(true);
			setLoading(false);

			setTimeout(() => {
				setShowError(false);
			}, 2000);
		} else {
			setData({
				...res,
				results: res?.results?.map((x: ResultsItem) => ({
					...x,
					poster_path: x?.poster_path ? `${imageURL}${x?.poster_path}` : null,
				})),
			});
			setLoading(false);
			handleLazyImage();
		}
	};

	const handleGetDetail = async (id: number) => {
		setLoading(true);

		const res = await getDetailMovie(id);

		if (!res?.success) {
			setData([]);
			setError(res?.errors);
			setShowError(true);
			setLoading(false);

			setTimeout(() => {
				setShowError(false);
			}, 2000);
		} else {
			setDetail({
				...res,
				poster_path: res?.poster_path ? `${imageURL}${res?.poster_path}` : null,
				backdrop_path: res?.backdrop_path
					? `${imageURL}${res?.backdrop_path}`
					: null,
			});
			setLoading(false);
			handleLazyImage();
		}
	};

	useEffect(() => {
		const layout = document.querySelector(".layout");
		if (layout) {
			layout.scrollTo({ top: 0 });
		}

		if (location?.pathname === "/") {
			navigate("/now-playing");
		}
		setShowError(false);
	}, [location]);

	return (
		<AppContext.Provider
			value={{
				viewType,
				setViewType,
				loading,
				handleGetData,
				data,
				error,
				showError,
				handleGetDetail,
				detail,
			}}
		>
			{children}
		</AppContext.Provider>
	);
};

export default AppProvider;
