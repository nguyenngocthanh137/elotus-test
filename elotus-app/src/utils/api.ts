import request from "./request";

import { convertObjectToParams } from "../helpers/convertObjectToParams";

import { RequestParams } from "../type/type";

export const getNowPlayingApi = async (params: RequestParams) => {
    const convertParams = convertObjectToParams(params);

    const res = await request(`/movie/now_playing?${convertParams}`, 'GET')
    return res
}

export const getTopRatedApi = async (params: RequestParams) => {
    const convertParams = convertObjectToParams(params);

    const res = await request(`/movie/top_rated?${convertParams}`, 'GET')
    return res
}

export const getSearchApi = async (params: RequestParams) => {
    const convertParams = convertObjectToParams(params);

    const res = await request(`/search/movie?${convertParams}`, 'GET')
    return res
}

export const getMovieDetail = async (id: number) => {
    const res = await request(`/movie?${id}`, 'GET')
    return res
}

export const getDetailMovie = async (id: number) => {
    const res = await request(`/movie/${id}`, 'GET')
    return res
}