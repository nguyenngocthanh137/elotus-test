import axios, { AxiosError } from "axios";

const baseURL = process.env.REACT_APP_API_URL;
const accessToken = process.env.REACT_APP_ACCESS_TOKEN;

const request = async (url: string, method: string) => {
    const headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${accessToken}`
    }

    const instance = axios.create({ headers })

    try {
        const res = await instance.request({
            method,
            url: `${baseURL}${url}`
        })

        return { ...res?.data, success: true }
    }
    catch (e) {
        const error = e as AxiosError;
        if (error?.response?.status === 404) {
            const newError = { ...error?.response?.data || {}, errors: ['There was an error ! Please try again !'] }
            return newError
        }
        else {
            return error?.response?.data
        }

    }
}

export default request