import { useContext } from "react";

import { Link } from "react-router-dom";

import { AppContext } from "../store/AppProvider";

import { ResultsItem } from "../type/type";

import classNames from "classnames";

type PropsType = {
	data?: ResultsItem;
};

const MovieItem: React.FC<PropsType> = ({ data }) => {
	const app = useContext(AppContext);

	return (
		<Link
			className={classNames("movie-item", { disabled: !data })}
			to={`/movie/${data?.id}`}
		>
			{!app?.loading ? (
				<img
					alt=""
					data-src={data?.poster_path}
					// loading="lazy"
					className="lazy-img"
				/>
			) : (
				<img alt="" />
			)}
			{!app?.loading ? (
				<div className="movie-content">
					<p className="movie-name">{data?.title}</p>
					<p className="movie-time">{data?.release_date}</p>
					<p className="movie-overview">{data?.overview}</p>
				</div>
			) : (
				<div className="movie-content">
					<div className="name-skeleton"></div>
					<div className="time-skeleton"></div>
					<div className="overview-skeleton"></div>
				</div>
			)}
		</Link>
	);
};

export default MovieItem;
