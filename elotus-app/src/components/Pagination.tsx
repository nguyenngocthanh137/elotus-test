import { useContext, useMemo } from "react";
import classnames from "classnames";
import { useParams } from "react-router-dom";

import { AppContext } from "../store/AppProvider";

type PropsType = {
	onPageChange: (value: number) => void;
};

const Pagination: React.FC<PropsType> = ({ onPageChange }) => {
	const params = useParams();
	const page = Number(params?.page) || 1;

	const app = useContext(AppContext);

	const data = app?.data;
	const loading = app?.loading;

	const DOTS = "...";

	const range = (start: number, end: number) => {
		let length = end - start + 1;
		return Array.from({ length }, (_, idx) => idx + start);
	};

	const paginationRange = useMemo(() => {
		const totalPageCount = data?.total_pages || 1;
		const siblingCount = 1;

		let paginationRange;

		const totalPageNumbers = siblingCount + 5;

		if (totalPageNumbers >= totalPageCount) {
			return range(1, totalPageCount);
		}

		const leftSiblingIndex = Math.max(page - siblingCount, 1);
		const rightSiblingIndex = Math.min(page + siblingCount, totalPageCount);

		const shouldShowLeftDots = leftSiblingIndex > 2;
		const shouldShowRightDots = rightSiblingIndex < totalPageCount - 2;

		const firstPageIndex = 1;
		const lastPageIndex = totalPageCount;

		if (!shouldShowLeftDots && shouldShowRightDots) {
			let leftItemCount = 3 + 2 * siblingCount;
			let leftRange = range(1, leftItemCount);

			paginationRange = [...leftRange, DOTS, totalPageCount];
		}

		if (shouldShowLeftDots && !shouldShowRightDots) {
			let rightItemCount = 3 + 2 * siblingCount;
			let rightRange = range(
				totalPageCount - rightItemCount + 1,
				totalPageCount
			);
			paginationRange = [firstPageIndex, DOTS, ...rightRange];
		}

		if (shouldShowLeftDots && shouldShowRightDots) {
			let middleRange = range(leftSiblingIndex, rightSiblingIndex);
			paginationRange = [
				firstPageIndex,
				DOTS,
				...middleRange,
				DOTS,
				lastPageIndex,
			];
		}

		return paginationRange;
	}, [data?.total_pages, page]);

	const onNext = () => {
		onPageChange(page + 1);
	};

	const onPrevious = () => {
		onPageChange(page - 1);
	};

	return (
		<ul className={classnames("pagination-container", { disabled: loading })}>
			<li
				className={classnames("pagination-item", {
					disabled: page === 1,
				})}
				onClick={onPrevious}
			>
				<div className="arrow left" />
			</li>
			{paginationRange?.map((pageNumber: number | string, index) => {
				if (pageNumber === DOTS) {
					return (
						<li className="pagination-item dots" key={index}>
							&#8230;
						</li>
					);
				}

				return (
					<li
						className={classnames("pagination-item", {
							selected: pageNumber === page,
						})}
						onClick={() => onPageChange(Number(pageNumber))}
						key={index}
					>
						{pageNumber}
					</li>
				);
			})}
			<li
				className={classnames("pagination-item", {
					disabled: page === data?.total_pages,
				})}
				onClick={onNext}
			>
				<div className="arrow right" />
			</li>
		</ul>
	);
};

export default Pagination;
