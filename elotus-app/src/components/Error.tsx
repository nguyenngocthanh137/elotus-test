import { useContext } from "react";
import classNames from "classnames";

import { AppContext } from "../store/AppProvider";

const Error = () => {
	const app = useContext(AppContext);
	return (
		<div
			className={classNames("error", {
				active: app?.showError,
			})}
		>
			{app?.error?.map((x, index) => (
				<p key={index}>{x}</p>
			))}
		</div>
	);
};

export default Error;
