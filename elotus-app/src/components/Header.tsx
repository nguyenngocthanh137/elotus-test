import { KeyboardEvent, useRef, LegacyRef, useEffect } from "react";
import classNames from "classnames";
import { Link, useLocation, useNavigate } from "react-router-dom";

import { PathType } from "../type/enum";

import { useQuery } from "../helpers/useQuery";

import { ReactComponent as Search } from "../assets/icon/search.svg";

const Header = () => {
	const location = useLocation();
	const navigate = useNavigate();

	const inputRef: LegacyRef<HTMLInputElement> | null = useRef(null);

	const queryData = useQuery(location);

	const query = queryData.get("query");

	const handleKeyboardEvent = (e: KeyboardEvent<HTMLInputElement>) => {
		if (e.keyCode === 13) {
			handleToDetail();
		}
	};

	const handleToDetail = () => {
		navigate({
			pathname: "/search",
			search: `?query=${inputRef?.current?.value}`,
		});
	};

	useEffect(() => {
		if (inputRef.current) {
			inputRef.current.value = query || "";
		}
	}, [query]);

	return (
		<div className="header">
			<div className="header-content">
				<div className="header-nav">
					<Link
						to={PathType.nowPlaying}
						className={classNames({
							active:
								location.pathname?.includes(PathType.nowPlaying) ||
								location.pathname === "/",
						})}
					>
						Now Playing
					</Link>
					<Link
						to={PathType.topRated}
						className={classNames({
							active: location.pathname?.includes(PathType.topRated),
						})}
					>
						Top Rated
					</Link>
				</div>
				<div className="header-search">
					<input
						placeholder="Search movie"
						onKeyUp={handleKeyboardEvent}
						ref={inputRef}
					/>
					<Search onClick={handleToDetail} />
				</div>
			</div>
		</div>
	);
};

export default Header;
