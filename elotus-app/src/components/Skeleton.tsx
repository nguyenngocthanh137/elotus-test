import classNames from "classnames";
import { useContext } from "react";

import { AppContext } from "../store/AppProvider";

import MovieItem from "./MovieItem";

import { ViewType } from "../type/enum";

const Skeleton = () => {
	const app = useContext(AppContext);

	return (
		<div
			className={classNames("movie-group", {
				"grid-group": app?.viewType === ViewType?.grid,
				"list-group": app?.viewType === ViewType?.list,
			})}
		>
			{Array?.from({ length: 20 })?.map((x, index) => (
				<MovieItem key={index} />
			))}
		</div>
	);
};

export default Skeleton;
