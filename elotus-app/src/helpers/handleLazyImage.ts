export const handleLazyImage = () => {
    setTimeout(() => {
        const observer = new IntersectionObserver(
            (entries, observer) => {
                entries.forEach((entry: IntersectionObserverEntry) => {
                    const target = entry.target as HTMLImageElement;
                    if (entry.intersectionRatio >= 0.6 && target.dataset.src) {
                        target.src = target.dataset.src;
                    }
                });
            },
            {
                threshold: 0.7,
            }
        );

        const elements = document.querySelectorAll(".lazy-img");

        elements.forEach((element) => {
            observer.observe(element);
        });
    });
};