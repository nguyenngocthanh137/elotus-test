export const convertObjectToParams = (
    obj: Record<string, string | number>
): string => {
    return new URLSearchParams(obj as unknown as URLSearchParams).toString();
};