export const useQuery = (location: { search: string }) => {
    return new URLSearchParams(location.search);
}