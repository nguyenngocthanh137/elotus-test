export enum ViewType {
    grid = 1,
    list = 2,
}

export enum PathType {
    nowPlaying = '/now-playing',
    topRated = '/top-rated',
    search = '/search',
    detail = '/detail'
}