export type AppContextType = {
    viewType: number;
    setViewType: React.Dispatch<React.SetStateAction<number>>;
    loading: boolean;
    handleGetData: (data: RequestData) => void;
    data: RequestDataList | null;
    error: string[];
    showError: boolean;
    handleGetDetail: (id: number) => void;
    detail: ResultsItem;
};

export type RequestData = {
    params: RequestParams;
    kind: string
}

export type RequestParams = {
    page: number | string;
    query?: string
}

export type ResultsItem = {
    title: string;
    release_date: string;
    poster_path: string;
    overview: string;
    id: number;
    backdrop_path: string;
    genres: Genres[];
    budget: number;
    vote_average: number;
    revenue: number;
    production_countries: Country[];
}

export type RequestDataList = {
    page: number;
    total_pages: number;
    results: ResultsItem[]
}

export type Genres = {
    name: string
}

export type Country = {
    name: string
}