import { Route, Routes } from "react-router-dom";

import ListMovie from "./pages/ListMovie";
import MovieDetail from "./pages/MovieDetail";
import PageError from "./pages/PageError";
import Layout from "./pages/Layout";

import "./assets/css/main.scss";

function App() {
	return (
		<Routes>
			<Route path="/" element={<Layout />}>
				<Route path="/now-playing/:page?" element={<ListMovie />}></Route>
				<Route path="/top-rated/:page?" element={<ListMovie />}></Route>
				<Route path="/search/:page?" element={<ListMovie />}></Route>
				<Route path="/movie/:id" element={<MovieDetail />}></Route>
			</Route>

			<Route path="*" element={<PageError />} />
		</Routes>
	);
}

export default App;
