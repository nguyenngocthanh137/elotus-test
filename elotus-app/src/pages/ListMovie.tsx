import { useParams, useLocation, useNavigate } from "react-router-dom";
import classNames from "classnames";
import { useContext, useEffect } from "react";

import { AppContext } from "../store/AppProvider";
import { useQuery } from "../helpers/useQuery";

import { ViewType, PathType } from "../type/enum";

import Pagination from "../components/Pagination";
import MovieItem from "../components/MovieItem";
import Skeleton from "../components/Skeleton";

import listIcon from "../assets/icon/list-icon.svg";
import gridIcon from "../assets/icon/grid-icon.svg";

const ListMovie = () => {
	const params = useParams();
	const location = useLocation();
	const navigate = useNavigate();

	const app = useContext(AppContext);
	const pathname = location.pathname;
	const page = params?.page;

	const queryData = useQuery(location);

	const query = queryData.get("query");

	const handleGetList = async () => {
		if (
			pathname?.includes(PathType.nowPlaying) ||
			pathname?.includes(PathType.topRated)
		) {
			app?.handleGetData({
				params: { page: page || 1 },
				kind: pathname?.includes(PathType.nowPlaying)
					? PathType.nowPlaying
					: PathType.topRated,
			});
		} else {
			app?.handleGetData({
				params: { page: page || 1, query: query || "" },
				kind: PathType.search,
			});
		}
	};

	useEffect(() => {
		handleGetList();
	}, [location]);

	const handlePageChange = (page: number) => {
		if (pathname?.includes(PathType.nowPlaying)) {
			navigate(`/now-playing/${page}`);
		} else if (pathname?.includes(PathType.topRated)) {
			navigate(`/top-rated/${page}`);
		} else {
			navigate({
				pathname: `/search/${page}`,
				search: `?query=${query}`,
			});
		}
	};

	return (
		<div className="body-content">
			<div className="body-content-list">
				<div className="view-group">
					<div
						onClick={() => app?.setViewType(ViewType?.grid)}
						className={classNames({
							active: app?.viewType === ViewType?.grid,
						})}
					>
						<img src={gridIcon} alt="grid" />
					</div>
					<div
						onClick={() => app?.setViewType(ViewType?.list)}
						className={classNames({
							active: app?.viewType === ViewType?.list,
						})}
					>
						<img src={listIcon} alt="list" />
					</div>
				</div>
				{!app?.loading ? (
					<div
						className={classNames("movie-group", {
							"grid-group": app?.viewType === ViewType?.grid,
							"list-group": app?.viewType === ViewType?.list,
						})}
					>
						{app?.data?.results?.map((x, index) => (
							<MovieItem data={x} key={index} />
						))}
					</div>
				) : (
					<Skeleton />
				)}
				{(!app?.data?.results || app?.data?.results?.length === 0) && (
					<p className="empty">No results were found</p>
				)}
			</div>
			{app?.data?.results && app?.data?.results?.length > 0 && (
				<Pagination onPageChange={handlePageChange} />
			)}
		</div>
	);
};

export default ListMovie;
