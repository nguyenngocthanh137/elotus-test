import { useParams } from "react-router-dom";
import { useContext, useEffect } from "react";

import { AppContext } from "../store/AppProvider";

const MovieDetail = () => {
	const params = useParams();

	const app = useContext(AppContext);

	const detail = app?.detail;
	const id = params?.id;

	useEffect(() => {
		if (id) {
			app?.handleGetDetail(Number(id));
		}
	}, []);

	return (
		<div className="body-detail">
			{!app?.loading ? (
				<img
					alt=""
					data-src={detail?.backdrop_path}
					loading="lazy"
					className="detail-banner lazy-img"
				/>
			) : (
				<img alt="" className="detail-banner" />
			)}
			<div className="detail-content">
				{!app?.loading ? (
					<div className="detail-content-col-1">
						<img
							alt=""
							data-src={detail?.poster_path}
							loading="lazy"
							className="detail-poster lazy-img"
						/>
						<div className="detail-vote">Rating {detail?.vote_average}</div>
						<button>Watch</button>
					</div>
				) : (
					<div className="detail-content-col-1">
						<img alt="" className="detail-poster" />
						<div className="vote-skeleton"></div>
						<div className="button-skeleton"></div>
					</div>
				)}
				{!app?.loading ? (
					<div className="detail-content-col-2">
						<p className="detail-title">{detail?.title}</p>
						<p className="detail-date">{detail?.release_date}</p>
						<div className="detail-money">
							<div>
								<p>Budget :</p>
								<p>{detail?.budget}</p>
							</div>
							<div>
								<p>Revenue :</p>
								<p>{detail?.revenue}</p>
							</div>
						</div>

						<div className="detail-genres">
							{detail?.genres?.map((x, index) => (
								<div className="genres-item" key={index}>
									{x?.name}
								</div>
							))}
						</div>
						<p className="detail-overview">{detail?.overview}</p>
					</div>
				) : (
					<div className="detail-content-col-2">
						<div className="title-skeleton"></div>
						<div className="date-skeleton"></div>
						<div className="detail-money">
							<div className="money-skeleton"></div>
							<div className="money-skeleton"></div>
						</div>
						<div className="detail-genres">
							{Array.from({ length: 3 })?.map((x, index) => (
								<div className="genres-item-skeleton" key={index}></div>
							))}
						</div>
						<div className="overview-skeleton"></div>
					</div>
				)}
			</div>
			{!app?.loading ? (
				<div className="detail-company">
					{detail?.production_countries?.map((x, index) => (
						<div key={index} className="company-item">
							{x?.name}
						</div>
					))}
				</div>
			) : (
				<div className="detail-company">
					{Array.from({ length: 3 })?.map((x, index) => (
						<div key={index} className="company-item-skeleton"></div>
					))}
				</div>
			)}
		</div>
	);
};

export default MovieDetail;
