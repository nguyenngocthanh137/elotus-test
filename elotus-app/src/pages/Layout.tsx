import { Outlet } from "react-router-dom";

import Error from "../components/Error";
import Header from "../components/Header";

const Layout = () => {
	return (
		<div className="layout">
			<Error />
			<Header />
			<div className="body">
				<Outlet />
			</div>
		</div>
	);
};

export default Layout;
